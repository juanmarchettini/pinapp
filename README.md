# PinApp
Skeleton: es desarrollog propio
Repo Gitlab https://gitlab.com/juanmarchettini/pinapp
Url Proyecto:
Documentacion:  http://200.45.208.55:8089/api/v1/docs
Endpoints: 
Get: http://200.45.208.55:8089/api/v1/clients
GET: http://200.45.208.55:8089/api/v1/clients/kpideclientes
Post: http://200.45.208.55:8089/api/v1/clients
         Body:
                 {
                   "name":"Juan",
                   "lastname":"Marchettini",
                   "birthdate":"1984-11-27"
                 }

## Tecnología usada:   Node.js + Express + Typescript + MongoDB (Mongoose) + Swagger
## Patron Arquitectonico: Se aplicó arquitectura hexagonal , para casi todos los casos, aplicando patrón de inyecciones.


## Getting started

Primer paso crear un archivo .env con los siguientes parametros:

- SERVER_PORT=8089
- SERVER_NAME= name_app

- //LOGGER
- LOGGER_ENABLE=true
- LOGGER_HAS_CONSOLE=true
- LOGGER_LEVEL=trace
- //MONGO
- MONGO_URI='mongodb://user:pass@localhost:27017/db'
- //JWT
- JWT_SECRET=changeThisKey
- JWT_EXPIRATION_TIME=2d

## Pasos a continuar

- Una vez clonado el repositorio realizar npm install
- Para realizar el build:  npm run build
- Para inicializar el servidor: npm run start
- Ingresar a ver la documentacion de la api a  http://localhost:8089/api/v1/docs


## NOTA IMPORTANTE!!

- **EL Parametro Edad (Age) pedido como ingreso , al tener la fecha de nacimiento se lo autocalcula.**