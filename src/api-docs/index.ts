import swaggerUi from 'swagger-ui-express'
import swaggerDocument from './swagger.json'

import swaggerClient from '../api/client/swaggerClient.json'

Object.assign(swaggerDocument.paths, swaggerClient.paths)
Object.assign(swaggerDocument.definitions, swaggerClient.definitions)

export const serve = swaggerUi.serve;
export const setup = swaggerUi.setup(swaggerDocument);