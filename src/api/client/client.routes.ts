import * as express from 'express';
import createClient from '../client/createClient';
import kpiClient from './kpiClients';
import listClients from '../client/listClients'

export const ClientRoutes = express.Router();
ClientRoutes.post('/', createClient);
ClientRoutes.get('/kpideclientes', kpiClient);
ClientRoutes.get('/', listClients);