import Validator from "../../core/validator";
import { TClient } from '../../hexagonal/entities/client.entity';
import UseCases from "../../hexagonal/useCases";
import LoggerManager, { LOGGER_VALUES } from "../../managers/Logger/logger.manager";
import moment from 'moment';

type body = {
    name: string,
    lastname: string,
    birthdate: Date
};

type response = {
    client: TClient
};

const bodyValidations = (body: body, res) => {
    const { name, lastname, birthdate } = body;
    if (Validator.isNullOrUndefined(name)) {
        throw new Error('The Name is invalid or null');
    }
    if (Validator.isNullOrUndefined(lastname)) {
        throw new Error('The LastName is invalid or null');

    }
    if (!moment(birthdate, 'YYYY-MM-DD', true).isValid()) {
        throw new Error('The date of birth is invalid. The format must be YYYY-MM-DD');
    }
}

const getResponse = (client: TClient): response => {
    return {
        client
    }
}

const controller = async (req, res) => {
    const body: body = req.body;
    const { name, lastname, birthdate } = body;
    try {
        bodyValidations(body, res);
        const birthdateDate = new Date(birthdate);
        const { data, error } = await UseCases.createClient({ name, lastname, birthdate: birthdateDate })
        if (error) {
            LoggerManager.error(LOGGER_VALUES.ERROR_400, { section: 'createClient.ts', title: 'Error Create Client', data: { name, lastname, birthdate }, error: LoggerManager.handleError(error) })
            return res.status(400).json({ msg: error.idErrorMessage, status: error.code });
        }
        const response: response = getResponse(data.client);
        return res.status(201).json({ data: response, status: '201' });
    } catch (error) {
        LoggerManager.error(LOGGER_VALUES.ERROR_400, { section: 'createClient.ts Catch', title: 'Error Create Client', data: { name, lastname, birthdate }, error: LoggerManager.handleError(error) })
        return res.status(400).json({ msg: error.message, status: '500' });
    }
};

export default controller;