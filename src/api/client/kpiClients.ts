import { TClient } from '../../hexagonal/entities/client.entity';
import UseCases from "../../hexagonal/useCases";
import LoggerManager, { LOGGER_VALUES } from "../../managers/Logger/logger.manager";

type response = {
    average: number,
    deviation: number
};

const getResponse = (average: number, deviation: number): response => {
    return {
        average,
        deviation
    }
}

const controller = async (req, res) => {
    try {
        const { data, error } = await UseCases.kpiClient({});
        if (error) {
            LoggerManager.error(LOGGER_VALUES.ERROR_400, { section: 'kpiClient.ts', title: 'Error Kpi Client', error: LoggerManager.handleError(error) })
            return res.status(400).json({ msg: error.idErrorMessage, status: error.code });
        }
        const { average, deviation } = data;
        const response: response = getResponse(average, deviation);
        return res.status(201).json({ data: response, status: '201' });
    } catch (error) {
        LoggerManager.error(LOGGER_VALUES.ERROR_400, { section: 'createClient.ts Catch', title: 'Error Kpi Client', error: LoggerManager.handleError(error) })
        return res.status(400).json({ msg: error.message, status: '500' });
    }
};

export default controller;