import ClientMongoRepository from '../../hexagonal/data-sources/mongo/client.ds';
//import { TClient } from '../../hexagonal/entities/client.entity';
import LoggerManager, { LOGGER_VALUES } from "../../managers/Logger/logger.manager";

type response = {
    "name":string,
    "lastname": string,
    "birthdate": Date,
    "deathDate": Date
}[];

const controller = async (req, res) => {
    try {
        const clientMongoRepository = new ClientMongoRepository();
        const { clients, error, errorMsg } = await clientMongoRepository.find({});
        if (error) {
            LoggerManager.error(LOGGER_VALUES.ERROR_400, { section: 'listClients.ts', title: 'Error on Clients List', error: LoggerManager.handleError(error) })
            return res.status(400).json({ msg: errorMsg, status: '400' });
        }
        const clientsWithDeathDate: response = clients.map((client) => {
            const deathDate = new Date(client.birthdate.getTime() + (78 * 365 * 24 * 60 * 60 * 1000)); // 78 es la edad promedio que tome para este caso
            const { name, lastname, birthdate } = client;
            return {
                name,
                lastname,
                birthdate,
                deathDate
            };
        });
        //console.log(clientsWithDeathDate);
        return res.status(201).json({ data: clientsWithDeathDate, status: '201' });
    } catch (error) {
        LoggerManager.error(LOGGER_VALUES.ERROR_500, { section: 'listClients.ts Catch', title: 'Error on Clients list', error: LoggerManager.handleError(error) })
        return res.status(500).json({ msg: error.message, status: '500' });
    }
};

export default controller;