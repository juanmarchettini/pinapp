export abstract class Config<T> {
    private config: T;

    constructor (values: T) {
        this.config = values;
    }

    getConfig() {
        return this.config;
    }
}