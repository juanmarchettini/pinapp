import { Config } from "./Config";
import { config as configDotEnv } from 'dotenv';

type ConfigType = {
    server: {
        port: number;
        name: string;
    },
    defaultLanguage: string,
    mongo: {
        uri: string,
    },
    jwt: {
        secretJwt: string,
        expirationTime: string,
    }
    logger: {
        enable: boolean;
        setting: {
            console: boolean;
            level: 'trace' | 'debug' | 'info' | 'warn' | 'error' | 'fatal' | string
        }
    }
}

export class DotEnvConfig extends Config<ConfigType> {
    constructor() {
        configDotEnv();
        const config: ConfigType = {
            server: {
                port: Number(process.env.SERVER_PORT),
                name: String(process.env.SERVER_NAME)
            },
            defaultLanguage: 'es',
            mongo: {
                uri: String(process.env.MONGO_URI || 'mongodb://localhost:27017/test?retryWrites=true&w=majority').trim(),
            },
            jwt: {
                secretJwt:  String(process.env.JWT_SECRET || 'changeThisKey'),
                expirationTime:  String(process.env.JWT_EXPIRATION_TIME|| '2d'),
            },
            logger: {
                enable: Boolean(process.env.LOGGER_ENABLE) || false,
                setting: {
                    console: Boolean(process.env.LOGGER_HAS_CONSOLE) || false,
                    level: String(process.env.LOGGER_LEVEL) || 'trace',
                }
            }

        }
        super(config);
    }
}