const Sanitizer = {
    sanitizeString: (value: string, options?: { notTrim?: boolean, notToLowerCase?: boolean, notRemoveInternalSpaces?: boolean }): string => {
        let finalValue = value;
        if(!options) {
            finalValue = String(finalValue).trim().replace(/ +/g, ' ').toLocaleLowerCase();
        } else {
            if(!options.notTrim) {
                finalValue = String(finalValue).trim();
            }
            if(!options.notToLowerCase) {
                finalValue = String(finalValue).toLocaleLowerCase();
            }
            if(!options.notTrim) {
                finalValue = String(finalValue).replace(/  +/g, ' ');
            }
        }
        return finalValue;
    },
    firstLetterToUpperCase: (str): string => {
        return str.toLowerCase().split(' ').map(function(word) {
            return word.replace(word[0], word[0].toUpperCase());
        }).join(' ');
    }
};

export default Sanitizer;