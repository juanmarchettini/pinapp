import Mongoose from 'mongoose';
import moment from 'moment';
import validator from 'validator';
const ObjectId = Mongoose.Types.ObjectId;

const Validator = {
    isNullOrUndefined: (value: any) => {
        return (value === undefined || value === null);
    },
    isEmptyString: (value) => {
        return (typeof value === 'string' && value === '');
    },
    isString: (value: any) => {
        return typeof value === 'string';
    },
    stringLengthMoreThan: (value: string, length: number) => {
        return String(value).length > length;
    },
    stringLengthBetweenInc: (value: string, minInc: number, maxInc: number, options?: { trim?: boolean }) => {
        let valueStringified = String(value);
        if (options && options.trim) {
            valueStringified = valueStringified.trim();
        }
        return (valueStringified.length >= minInc && valueStringified.length <= maxInc);
    },
    isValidEmail: (email: string) => {
        return validator.isEmail(String(email));
    },
    isObject: (obj: any) => {
        return typeof obj === 'object' && obj !== null && !Array.isArray(obj);
    },
    isBoolean: (value: any) => {
        return typeof value === 'boolean';
    },
    isMongooseId: (value: any) => {
        return ObjectId.isValid(value);
    },
    isArray: (value: any) => {
        return Array.isArray(value);
    },
    isArrayOfString: (arr: any) => {
        if(!Array.isArray(arr)) {
            return false;
        }
        for (let i = 0; i < arr.length; i++) {
            if(!Validator.isString(arr[i])) {
                return false;
            }
        }
        return true;
    },
    isNumberGreaterEqualThan: (num: number, min) => {
        return (typeof num === 'number' && num >= min);
    },
    isNumberBetweenInc: (num: number, min: number, max: number) => {
        return (typeof num === 'number' && num >= min && num <= max);
    },
    isNaturalNumber: (num: number) => {
        if (typeof num !== 'number') {
            return false;
        } else {
            return (num >= 0.0) && (Math.floor(num) === num) && num !== Infinity;
        }
    },
    isValidDateDDMMYYYY: (dateString: string): boolean => {
        const re = new RegExp(/^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$/, 'i'); // notacion literal
        if (re.test(dateString)) {
            const splittedDate = dateString.split('/');
            const dateN = Number(splittedDate[0]);
            const monthN = Number(splittedDate[1]);
            const yearN = Number(splittedDate[2]);
            const d1 = new Date(yearN, monthN - 1, dateN, 12, 0, 0, 0);
            if (d1.getDate() !== dateN || d1.getMonth() !== (monthN - 1) || d1.getFullYear() !== yearN) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    },
    isValidHourHHMM: (hourString: string): boolean => {
        if (typeof hourString !== 'string') return false;
        const re = new RegExp(/^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$/, 'i'); // notacion literal
        return re.test(hourString);
    },
    isValidUTCZFormat: (dateUTC: string, options?: { withoutMS?: boolean, withoutHHSS?: boolean }) => {
        if (typeof dateUTC !== 'string') return false;
        if (options && options.withoutMS) {
            // same re but without the miliseconds '.000';    
            const re = new RegExp(/\b[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}Z\b/, 'i'); // notacion literal
            return re.test(dateUTC);
        }

        if(options && options.withoutHHSS) {
            const re = new RegExp(/\b[0-9]{4}-[0-9]{2}-[0-9]{2}\b/, 'i'); //formato normal -> mongo lo guarda como UTC
            return re.test(dateUTC)
        }

        const re = new RegExp(/\b[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{3}Z\b/, 'i'); // notacion literal
        return re.test(dateUTC);
    },
    isValidUTCZDate: (dateUTC: string) => {
        const d = new Date(dateUTC);
        const splittedDate = dateUTC.split('T')[0].split('-');
        const year = Number(splittedDate[0]);
        const monthIndex = Number(splittedDate[1]) - 1;
        const date = Number(splittedDate[2]);

        const isoStringDate = d.toISOString();
        const splittedDateISO = isoStringDate.split('T')[0].split('-');
        const yearISO = Number(splittedDateISO[0]);
        const monthIndexISO = Number(splittedDateISO[1]) - 1;
        const dateISO = Number(splittedDateISO[2]);
        if (yearISO !== year || monthIndexISO !== monthIndex || dateISO !== date) {
            return false;
        }
       
        return true;
    },
    isValidStringDate: (dateString: string): boolean => {
        if (typeof dateString !== 'string') return false;
       
        if(!Validator.isValidDate(new Date(dateString))) {
            return false;
        }
        return moment(dateString).isValid();
    },
    isValidDate: d => {
        if (Object.prototype.toString.call(d) === "[object Date]") {
            // it is a date
            if (isNaN(d.getTime())) {  // d.valueOf() could also work
                // date is not valid
                return false;
            } else {
                // date is valid
                return true;
            }
        } else {
            // not a date
            return false;
        }
    }
};

export default Validator;