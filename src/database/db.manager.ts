const mongoose = require('mongoose');
import { DotEnvConfig } from "../config/DonEnvConfig";
import LoggerManager from "../managers/Logger/logger.manager";
import { LOGGER_VALUES } from "../managers/Logger/logger.types";

export default class DBManager {

    static async connect(): Promise<any> {
        const config = new DotEnvConfig();     
        let uri = config.getConfig().mongo.uri;
        try {
            const connection = await mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false });
           // DBManager.registerModels();
        } catch (err) {
            LoggerManager.error(LOGGER_VALUES.DB_CONNECTION, { section: 'db.manager.ts', title: 'Error on MongoDB connection',mongo_uri:uri,  error: LoggerManager.handleError(err) });
            throw 'Error on connecting DB';
        }

    }

    static async registerModels(): Promise<any> {
        require('../hexagonal/data-sources/mongo/schemes/client.schema');        
    }

}
