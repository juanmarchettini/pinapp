// GENERAL FUNCTIONS FOR USE CASES
export type UC_Error = {
    error?: boolean;
    idErrorMessage?: string;
    code?: string;
    formatted?: { values: any };
    errorType: 'UC_ERROR';
};

export const generateUCError = (
    idErrorMessage: string,
    code: string,
    formatted?: {
        values: any
    }
): UC_Error => {
    return {
        error: true,
        idErrorMessage,
        code,
        errorType: 'UC_ERROR',
        formatted
    };
};

export const handleUCError = (error) => {
    return {
        error: (error?.errorType === 'UC_ERROR')
            ? generateUCError(error.idErrorMessage, error.code)
            : generateUCError('@HuboUnError', 'error')
    };
};
