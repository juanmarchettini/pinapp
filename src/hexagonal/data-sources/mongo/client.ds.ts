
import { TClient } from "../../entities/client.entity";
import Client from "./schemes/client.schema";
import { IClientRepository } from '../../repositories/client.repository';
import Sanitizer from "../../../core/sanitizer";

class ClientMongoRepository implements IClientRepository {

    public async find(
        query,
        options?: {
            select?: string[]
        }
    ): Promise<{
        error?: boolean,
        errorMsg?: string
        clients: TClient[];
    }> {
        try {
            const promise = Client.find(query);
            if (options) {
                if (options.select) {
                    options.select.map(s => {
                        promise.select(s);
                    });
                }
            }

            const clients: any = await promise.exec();
            return {
                clients
            };
        } catch (error) {
            return {
                error: true,
                errorMsg: error,
                clients: []
            };
        }
    }

    public async createClient(clientData: { lastname: string, name: string, birthdate: Date }, options?: { session?: any }): Promise<{ client: TClient }> {
        const newClient = new Client({
            name: Sanitizer.sanitizeString(clientData.name),
            lastname: Sanitizer.sanitizeString(clientData.lastname),
            birthdate: clientData.birthdate
        });
        let opt: any = {};
        if (options && options.session) {
            opt.session = options.session;
        }
        const savedClient: any = await newClient.save(opt);
        return {
            client: savedClient
        };
    }

}

export default ClientMongoRepository;