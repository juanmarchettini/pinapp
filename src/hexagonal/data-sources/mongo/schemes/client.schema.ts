import { Schema, model, Document } from 'mongoose';
import schemaHelper from '../../../shared/schema';

const clientSchema: Schema = new Schema({
    lastname: {
        type: String
    },
    name: {
        type: String
    },
    email: {
        type: String,
        require: false
    },
    birthdate: {
        type: Date
    },
    createdAt:{
        type: Date
    },
    updatedAt: {
        type: Date
    }
}, { versionKey: false, timestamps: true });

export default model('Client', clientSchema);