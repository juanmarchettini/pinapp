export type TClient = {
    _id?: string;
    name: string;
    lastname: string;
    email?: string;
    birthdate: Date;
    age?: number,
    createdAt?: Date;
    updatedAt?: Date;
};
