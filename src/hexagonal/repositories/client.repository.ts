import { TClient } from "../entities/client.entity";


export interface IClientRepository {

    find(query, options?: { select?: string[] }): Promise<{ clients: TClient[], error?:boolean, errorMsg?:string }>;
    createClient(clientData: { lastname: string, name: string, birthdate: Date }, options?: { session?: any }): Promise<{ client: TClient }>

}