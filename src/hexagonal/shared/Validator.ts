// const Mongoose = require('mongoose');
// const ObjectId = Mongoose.Types.ObjectId;

export interface IValidator {
    isNullOrUndefined(value: any): boolean;
    isString(value: any): boolean;
    checkIfStringDateUTCIsValidDate(UTCDateStringified: string): boolean;
    // isMongooseId(value: any): boolean;
    isNaturalNumber(num: number): boolean;
};

class Validator implements IValidator {

    isNullOrUndefined(value: any): boolean {
        return (value === undefined || value === null);
    }

    isString(value: any): boolean {
        return typeof value === 'string';
    }

    checkIfStringDateUTCIsValidDate(UTCDateStringified: string): boolean {
        const re = new RegExp(/^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{3}Z$/, 'i'); // notacion literal
        if (!re.test(UTCDateStringified)) {
            return false;
        }
        const splitted = UTCDateStringified.split('T');
        const date = splitted[0];
        const time = splitted[1];
        const splittedDate = date.split('-');
        const year = Number(splittedDate[0]);
        const monthIndex = Number(splittedDate[1]) - 1;
        const day = Number(splittedDate[2]);

        const splittedTime = time.split(':');
        const hours = Number(splittedTime[0]);
        const minutes = Number(splittedTime[1]);
        const seconds = Number(splittedTime[2].split('.')[0]);
        const millisecondsWithZ = splittedTime[2].split('.')[1];
        const milliseconds = Number(millisecondsWithZ.substring(0, millisecondsWithZ.length - 1));
        const d = new Date(UTCDateStringified);
        if (
            d.getUTCFullYear() !== year ||
            d.getUTCMonth() !== monthIndex ||
            d.getUTCDate() !== day ||
            d.getUTCHours() !== hours ||
            d.getUTCMinutes() !== minutes ||
            d.getUTCSeconds() !== seconds ||
            d.getUTCMilliseconds() !== milliseconds
        ) {
            return false;
        }

        return true;
    }

    // isMongooseId(value: any): boolean {
    //     return ObjectId.isValid(value);
    // }

    isNaturalNumber(num: number): boolean {
        if (typeof num !== 'number') {
            return false;
        } else {
            return (num >= 0.0) && (Math.floor(num) === num) && num !== Infinity;
        }
    }
}

export default Validator;