import { Schema } from "mongoose";

const schemaHelper = {
    updateOnSave: (schema: Schema) => {
        schema.pre('save', function (next) {
            //@ts-ignore
            this.updatedAt = Date.now();
            next();
        });
    }
}

export default schemaHelper