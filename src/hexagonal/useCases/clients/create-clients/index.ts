import { generateUCError, handleUCError, UC_Error } from "../../../core-hex";
import { TClient } from '../../../entities/client.entity';
import { IClientRepository } from "../../../repositories/client.repository";
import { IValidator } from "../../../shared/Validator";

type TParams = {
    name: string,
    lastname: string,
    birthdate: Date,
};

type TOptions = {
    session: any;
};

type dataResponse = {
    client: TClient
};

type UC_Response = {
    data?: dataResponse;
    error?: UC_Error;
};

type IRepositories = {
    Validator: IValidator;
    ClientRepository: IClientRepository;
};

export type I_UC_CreateClient = (params: TParams, options?: TOptions) => Promise<UC_Response>;

export default class CreateClient {
    private codeBase = 'uc-CreateClient';
    private Validator: IValidator;
    private ClientRepository: IClientRepository;

    constructor(
        Repositories: IRepositories,
    ) {
        this.Validator = Repositories.Validator;
        this.ClientRepository = Repositories.ClientRepository;
    }

    private paramsValidations(params: TParams): { validParams: any } {
        let { name, lastname, birthdate } = params;
        let validParams = true;
        if (!this.Validator.isString(name)){
            throw generateUCError('The name parameter is invalid', `${this.codeBase}-1`);
        }
        if (!this.Validator.isString(lastname)){
            throw generateUCError('the lastname parameter is invalid', `${this.codeBase}-2`);
        }
        if (this.Validator.isNullOrUndefined(birthdate)){
            throw generateUCError('the name birthdate is invalid', `${this.codeBase}-3`);
        }

        return {
            validParams
        }
    }

    async execute(params: TParams, options?: TOptions): Promise<UC_Response> {
        const { name, lastname, birthdate } = params;
        let session = options?.session || null;
        try {
            this.paramsValidations(params);
            const { client } = await this.ClientRepository.createClient({lastname, name, birthdate}, {session});
            return {
                data: { client }
            };
        } catch (error) {
            return handleUCError(error);
        }
    }
}