import { generateUCError, handleUCError, UC_Error } from "../../../core-hex";
import { IClientRepository } from "../../../repositories/client.repository";
import { IValidator } from "../../../shared/Validator";


type TOptions = {
    session: any;
};

type dataResponse = {
    average: number,
    deviation: number
};

type UC_Response = {
    data?: dataResponse;
    error?: UC_Error;
};

type IRepositories = {
    Validator: IValidator;
    ClientRepository: IClientRepository;
};

export type I_UC_KpiClient = (params, options?: TOptions) => Promise<UC_Response>;

export default class KpiClient {
    private codeBase = 'uc-KpiClient';
    private Validator: IValidator;
    private ClientRepository: IClientRepository;

    constructor(
        Repositories: IRepositories,
    ) {
        this.Validator = Repositories.Validator;
        this.ClientRepository = Repositories.ClientRepository;
    }

    private getAge(birthdate: Date): number {
        const ageInMilliseconds = Date.now() - birthdate.getTime();
        const ageInYears = ageInMilliseconds / (1000 * 60 * 60 * 24 * 365);
        return Math.floor(ageInYears);
    }

    private calculateAverage(ages: number[]): number {
        const sum = ages.reduce((acc, age) => acc + age, 0);
        return sum / ages.length;
    }

    private calculateDeviation(ages: number[], average: number): number {
        const variance = ages.reduce((acc, age) => acc + Math.pow(age - average, 2), 0) / ages.length;
        return Math.sqrt(variance);
    }

    async execute(params, options?: TOptions): Promise<UC_Response> {
        try {
            let average = 0;
            let deviation = 0;
            const { clients, error, errorMsg } = await this.ClientRepository.find({});
            if (clients && clients.length>0) {
                const ages = clients.map((client) => this.getAge(client.birthdate));
                average = this.calculateAverage(ages);
                deviation = this.calculateDeviation(ages, average);
            }
            return { data: { average, deviation } };
        } catch (error) {
            return handleUCError(error);
        }
    }
}