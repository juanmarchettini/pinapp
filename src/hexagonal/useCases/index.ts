
import ClientMongoRepository from '../data-sources/mongo/client.ds';
import Validator from '../shared/Validator';
const ValidatorRepository = new Validator();
 
//Import Use Cases
import CreateClient, { I_UC_CreateClient } from './clients/create-clients/index';
import KpiClient, { I_UC_KpiClient } from './clients/kpi';


//Import Repository
const ClientRepository = new ClientMongoRepository();



// Use cases
interface IUseCases {
    createClient?: I_UC_CreateClient,
    kpiClient?: I_UC_KpiClient
}

const UseCases: IUseCases = {};

//Inyeccion
const makerCreateClient = () => {
    const instance = new CreateClient({
        ClientRepository,
        Validator: ValidatorRepository
    });
    return async (params, options) => {
        return await instance.execute(params, options);
    };
};
UseCases.createClient = makerCreateClient();

const makerkpiClient = () => {
    const instance = new KpiClient({
        ClientRepository,
        Validator: ValidatorRepository
    });
    return async (params, options) => {
        return await instance.execute(params, options);
    };
};
UseCases.kpiClient = makerkpiClient();

export default UseCases;