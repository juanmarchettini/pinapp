import { DotEnvConfig } from "./config/DonEnvConfig";
import { ExpressServer } from "./server/ExpressServer";
import LoggerManager from "./managers/Logger/logger.manager";
import { LOGGER_VALUES } from "./managers/Logger/logger.types";
import DBManager from "./database/db.manager";

const init = async () => {
    try {
        const config = new DotEnvConfig();
        const server = new ExpressServer({
            port: config.getConfig().server.port,
            name: config.getConfig().server.name,
        });
        await DBManager.connect();
        server.launchServer();
        await LoggerManager.init();
    } catch (error) {
        LoggerManager.error(LOGGER_VALUES.ERROR_500, { section: 'index.ts', title: `Error`, error: LoggerManager.handleError(error) });
    }

};

init();