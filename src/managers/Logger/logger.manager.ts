const bunyan = require('bunyan');
import { DotEnvConfig } from '../../config/DonEnvConfig';

const getLevel = (level) => {
    if (['trace', 'debug', 'info', 'warn', 'error', 'fatal'].includes(level)) {
        return level;
    } else {
        return 'info';
    }
};

const _getErrorValue = (value) => {
    let val;
    if (value?.valueOf && typeof value.valueOf === 'function') {
        val = value.valueOf();
    } else {
        val = value;
    }
    return val;
}

const handleLoggerConsole = (obj, value: any) => {
    console.log(value, obj)
}

const LoggerManager = {
    // Levels
    // "fatal" (60): The service/app is going to stop or become unusable now. An operator should definitely look into this soon.
    // "error" (50): Fatal for a particular request, but the service/app continues servicing other requests. An operator should look at this soon(ish).
    // "warn" (40): A note on something that should probably be looked at by an operator eventually.
    // "info" (30): Detail on regular operation.
    // "debug" (20): Anything else, i.e. too verbose to be included in "info" level.
    // "trace" (10): Logging from external libraries used by your app or very detailed application logging.

    logger: null,
    enabled: false,
    // static currentConfig: any;
    async init() {
        const configFromDB = new DotEnvConfig();
        LoggerManager.enabled = configFromDB.getConfig().logger.enable;

        const hasConsoleLog = configFromDB.getConfig().logger.setting.console;
        if (hasConsoleLog) {
            // addConsoleHandlesToLogger(LoggerManager);
            LoggerManager.logger = {
                trace: handleLoggerConsole,
                debug: handleLoggerConsole,
                info: handleLoggerConsole,
                warn: handleLoggerConsole,
                error: handleLoggerConsole,
                fatal: handleLoggerConsole
            }
        } else {
            try {
                LoggerManager.logger = bunyan.createLogger({
                    name: 'api-logger',
                    streams: [
                        {
                            stream: process.stdout,
                            level: getLevel(configFromDB.getConfig().logger.setting.level)
                        }
                    ]
                });
            } catch (error) {
                console.log(error);
            }

        }
    },

    // level 10
    trace(value: any, obj: object = {}) {
        if (LoggerManager.enabled) {
            LoggerManager.logger.trace(obj, value);
        }
    },

    // level 20
    debug(value: any, obj: object = {}) {
        if (LoggerManager.enabled) {
            LoggerManager.logger.debug(obj, value);
        }
    },

    // level 30
    info(value: any, obj: object = {}) {
        if (LoggerManager.enabled) {
            LoggerManager.logger.info(obj, value);
        }
    },

    // level 40
    warn(value: any, obj: object = {}) {
        if (LoggerManager.enabled) {
            LoggerManager.logger.warn(obj, value);
        }
    },

    // level 50
    error(value: any, obj: object = {}) {
        if (LoggerManager.enabled) {
            const val = _getErrorValue(value);
            LoggerManager.logger.error(obj, val);
        }
    },

    // level 60
    fatal(value: any, obj: object = {}) {
        if (LoggerManager.enabled) {
            const val = _getErrorValue(value);
            LoggerManager.logger.fatal(obj, val);
        }
    },

    handleError(error) {
        let value;
        if (error.stack) {
            value = error.stack;
        } else {
            if (typeof error === 'object') {
                value = JSON.stringify(error);
            } else {
                value = String(error);
            }
        }
        return value;
    }
};

export default LoggerManager;

export enum LOGGER_VALUES {
    START_SERVER = 'START SERVER',
    ERROR_500 = 'ERROR 500',
    ERROR_400 = 'ERROR 400',
    AUTH_401 = 'AUTH 401',
    AUTH_403 = 'AUTH 403',
    JOBS = 'JOBS',
    CONFIG = 'CONFIG',
    MONGO = 'MONGO',
};

export enum LOGGER_SECTION {
    CONFIG = ' | CONFIG '
}