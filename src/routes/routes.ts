import * as express from 'express';

import { ClientRoutes } from '../api/client/client.routes';

import {serve, setup} from '../api-docs/index'

const apiRoutes = express.Router();

apiRoutes.use('/clients', ClientRoutes);

//documentar
apiRoutes.use('/docs', serve, setup);

export default apiRoutes;