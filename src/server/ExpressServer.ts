import express, { Request, Response } from 'express';
import bodyParser from 'body-parser';
import { Server } from "./Server";
import LoggerManager from '../managers/Logger/logger.manager';
import { LOGGER_VALUES } from '../managers/Logger/logger.types';
import apiRoutes from '../routes/routes';
import cors from 'cors';

type constructorDataType = {
    port: number;
    name: string;
};

export class ExpressServer extends Server {

    private app: any;
    private serverConfig: {
        port: number;
        name: string;
    };

    constructor(values: constructorDataType) {
        super();
        this.serverConfig = {
            port: values.port,
            name: values.name
        };

        this.init();
        this.addGlobalMiddlewares();
    }

    private init() {
        this.app = express();
    }

    private addGlobalMiddlewares() {
        //this.app.use(jwtMiddleware());
        this.app.use(bodyParser.json({ limit: '50mb' }));
        this.app.use(cors());
        // This is to handle a bad-format body json
        this.app.use((error, req, res, next) => {
            if (error instanceof SyntaxError) {
                throw new Error("SYNTAX ERRROR");
            } else {
                next();
            }
        });
    }

    public launchServer() {
        const port = this.serverConfig.port;
        const name = this.serverConfig.name;
        this.app.use('/api/v1', apiRoutes);

        this.app.listen(port, () => {
            LoggerManager.info(LOGGER_VALUES.START_SERVER, { title: `Server: ${name} listening at http://localhost:${port}` });
        });
    }
}

function createLocaleMiddleware(arg0: { default: string; }): any {
    throw new Error('Function not implemented.');
}
