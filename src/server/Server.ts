export abstract class Server {
    abstract launchServer(data: { port: number; name: string; });
};